package controller;

import java.util.Date;

import dao.TransactionDAOImp;
import model.Transaction;

public class TransactionController {
  
	Transaction transaction;
	TransactionDAOImp imp = new TransactionDAOImp();
	
	public int addTransaction(Long account_number, Long transferred_amount, String narration, Date date_of_transaction,
			String status, String rid) {
				transaction = new Transaction(account_number, transferred_amount, narration, date_of_transaction, status, rid);
				return imp.addTransaction(transaction);
			}
}
