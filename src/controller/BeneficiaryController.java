package controller;

import java.util.List;

import dao.BeneficiaryDAOImp;

import model.Beneficiary;

public class BeneficiaryController {
	
	Beneficiary beneficiary;
	BeneficiaryDAOImp imp = new BeneficiaryDAOImp();
	
	public  int addBeneficiary(Long account_number,String name, String account_type, String account_status,
			                   String IfSCcode,String email,Long max_limit,String rid) {
	beneficiary = new Beneficiary(account_number,name,account_type,account_status,IfSCcode,email,max_limit,rid);
		return imp.addBenficiary(beneficiary);
	}
	public List<Beneficiary> readBeneficiary(){
		return imp.readBeneficiary();
	}
public int updateBeneficiary(String name,String account_type,String rid) {
	beneficiary = new Beneficiary();
	beneficiary.setName(name);
	beneficiary.setAccounttype(account_type);
	beneficiary.setRid(rid);
	return imp.updateBeneficiary(beneficiary);
}
public int deleteBeneficiary(String rid) {
	beneficiary = new Beneficiary();
		beneficiary.setRid(rid);
		return imp.deleteBeneficiary(beneficiary);
	}
}

