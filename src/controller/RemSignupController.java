package controller;

import dao.RemSignupDAOImp;
import model.Remittersignup;

public class RemSignupController {
  Remittersignup signup;
  RemSignupDAOImp imp = new RemSignupDAOImp(); 

  public int addRemSignup(String rid, String name, Long accountnumber, Long accountbalance,String accounttype,
		  String password,String role) {
	  signup = new Remittersignup(rid, name, accountnumber, accountbalance, accounttype, password, role);
			  return imp.addRemSignup(signup);
  }
  
  public int updateRemSignup(String rid,String name,Long accountbalance,String password) {
	  signup = new Remittersignup();
	  signup.setRid(rid);
	  signup.setName(name);
	  signup.setAccount_balance(accountbalance);
	  signup.setPassword(password);
	  return imp.updateRemSignup(signup);
  }


}



