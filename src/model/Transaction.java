package model;

import java.util.Date;

public class Transaction {
    
	private Long account_number;
	private Long transferred_amount;
	private String narration;
	private Date date_of_transaction;
	private String status;
	private String rid;
	public Transaction(Long account_number, Long transferred_amount, String narration, Date date_of_transaction,
			String status, String rid) {
		super();
		this.account_number = account_number;
		this.transferred_amount = transferred_amount;
		this.narration = narration;
		this.date_of_transaction = date_of_transaction;
		this.status = status;
		this.rid = rid;
	}
	public Long getAccount_number() {
		return account_number;
	}
	public void setAccount_number(Long account_number) {
		this.account_number = account_number;
	}
	public Long getTransferred_amount() {
		return transferred_amount;
	}
	public void setTransferred_amount(Long transferred_amount) {
		this.transferred_amount = transferred_amount;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public Date getDate_of_transaction() {
		return date_of_transaction;
	}
	public void setDate_of_transaction(Date date_of_transaction) {
		this.date_of_transaction = date_of_transaction;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRid() {
		return rid;
	}
	public void setRid(String rid) {
		this.rid = rid;
	}
	@Override
	public String toString() {
		return "Transaction [account_number=" + account_number + ", transferred_amount=" + transferred_amount
				+ ", narration=" + narration + ", date_of_transaction=" + date_of_transaction + ", status=" + status
				+ ", rid=" + rid + "]";
	}
	
	
}
