package model;

public class RemPersonal {
	private String rid;
	private String email;
	private Long mobile;
	private String address;
	
	public RemPersonal() {
	}

	public RemPersonal(String rid, String email, Long mobile, String address) {
		super();
		this.rid = rid;
		this.email = email;
		this.mobile = mobile;
		this.address = address;
	}

	public String getRid() {
		return rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "RemPersonal [rid=" + rid + ", email=" + email + ", mobile=" + mobile + ", address="
				+ address + "]";
	}
	
	
}
	
