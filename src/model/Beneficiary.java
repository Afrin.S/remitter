package model;

public class Beneficiary {
 private Long accountnumber;
 private String name;
 private String accounttype;
 private String accountstatus;
 private String IFSCcode;
 private String email;
 private Long maxlimit;
 private String rid;
  public Beneficiary() {
	  
  }
public Beneficiary(Long accountnumber, String name, String accounttype, String accountstatus, String iFSCcode,
		String email, Long maxlimit, String rid) {
	super();
	this.accountnumber = accountnumber;
	this.name = name;
	this.accounttype = accounttype;
	this.accountstatus = accountstatus;
	IFSCcode = iFSCcode;
	this.email = email;
	this.maxlimit = maxlimit;
	this.rid = rid;
}
public Long getAccountnumber() {
	return accountnumber;
}
public void setAccountnumber(Long accountnumber) {
	this.accountnumber = accountnumber;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getAccounttype() {
	return accounttype;
}
public void setAccounttype(String accounttype) {
	this.accounttype = accounttype;
}
public String getAccountstatus() {
	return accountstatus;
}
public void setAccountstatus(String accountstatus) {
	this.accountstatus = accountstatus;
}
public String getIFSCcode() {
	return IFSCcode;
}
public void setIFSCcode(String iFSCcode) {
	IFSCcode = iFSCcode;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public Long getMaxlimit() {
	return maxlimit;
}
public void setMaxlimit(Long maxlimit) {
	this.maxlimit = maxlimit;
}
public String getRid() {
	return rid;
}
public void setRid(String rid) {
	this.rid = rid;
}
@Override
public String toString() {
	return "Beneficiary [accountnumber=" + accountnumber + ", name=" + name + ", accounttype=" + accounttype
			+ ", accountstatus=" + accountstatus + ", IFSCcode=" + IFSCcode + ", email=" + email + ", maxlimit="
			+ maxlimit + ", rid=" + rid + "]";
}
  
}
