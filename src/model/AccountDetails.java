package model;

public class AccountDetails {
	
	
		private Long accountnumber;
		
		public AccountDetails() {
	}

		public AccountDetails(Long accountnumber) {
			super();
			this.accountnumber = accountnumber;
		}

		public Long getAccountnumber() {
			return accountnumber;
		}

		public void setAccountnumber(Long accountnumber) {
			this.accountnumber = accountnumber;
		}

		@Override
		public String toString() {
			return " accountnumber:" + accountnumber ;
		}

		
}