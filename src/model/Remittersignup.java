package model;

public class Remittersignup {

	private String rid;
	private String name;
	private Long account_number;
	private Long account_balance;
	private String account_type;
	private String password;
	private String role;
	
	public Remittersignup() {
		
	}

	public Remittersignup(String rid, String name, Long account_number, Long account_balance, String account_type,
			String password, String role) {
		super();
		this.rid = rid;
		this.name = name;
		this.account_number = account_number;
		this.account_balance = account_balance;
		this.account_type = account_type;
		this.password = password;
		this.role = role;
	}

	public String getRid() {
		return rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAccount_number() {
		return account_number;
	}

	public void setAccount_number(Long account_number) {
		this.account_number = account_number;
	}

	public Long getAccount_balance() {
		return account_balance;
	}

	public void setAccount_balance(Long account_balance) {
		this.account_balance = account_balance;
	}

	public String getAccount_type() {
		return account_type;
	}

	public void setAccount_type(String account_type) {
		this.account_type = account_type;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "Remittersignup [rid=" + rid + ", name=" + name + ", account_number=" + account_number
				+ ", account_balance=" + account_balance + ", account_type=" + account_type + ", password=" + password
				+ ", role=" + role + "]";
	}

}