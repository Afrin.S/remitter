package dao;


import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.AccountDetails;
import util.Db;
import util.Query;

public class AccountDAOImp implements IAccountDAO {
	
	int result;
	PreparedStatement pst;
		@Override
		public int addAccount(AccountDetails details) {
			try {
				pst=Db.getDbProperties().prepareStatement(Query.addAccount);
				pst.setLong(1, details.getAccountnumber());
				result=pst.executeUpdate();
			} catch (SQLException e) {
				System.err.println("addAccount" +e.getMessage());
				e.printStackTrace();
			}
			return result;
		}

	}

