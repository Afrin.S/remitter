package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Beneficiary;
import util.Db;
import util.Query;

public class BeneficiaryDAOImp implements IBenficiaryDAO {
	
		int result;
		PreparedStatement pst;
		
		@Override
		public int addBenficiary(Beneficiary beneficiary) {
			try {
			pst=Db.getDbProperties().prepareStatement(Query.addBeneficiary);
			pst.setLong(1, beneficiary.getAccountnumber());
			pst.setString(2, beneficiary.getName());
			pst.setString(3, beneficiary.getAccounttype());
			pst.setString(4, beneficiary.getAccountstatus());
			pst.setString(5, beneficiary.getIFSCcode());
			pst.setString(6, beneficiary.getEmail());
			pst.setLong(7, beneficiary.getMaxlimit());
			pst.setString(8, beneficiary.getRid());
			result=pst.executeUpdate();
		} catch (SQLException e) {
			System.err.println("addBeneficiary" +e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}
		@Override
		public List<Beneficiary> readBeneficiary() {
			List<Beneficiary> list = new ArrayList<Beneficiary>();
			try {
				pst=Db.getDbProperties().prepareStatement(Query.readBeneficiary);
				ResultSet rs = pst.executeQuery();
				while(rs.next()) {
					Beneficiary beneficiary = new Beneficiary(rs.getLong(1),rs.getString(2),rs.getString(3),
							rs.getString(4),rs.getString(5),rs.getString(6),rs.getLong(7),rs.getString(8));
					list.add(beneficiary);
				}
			}catch(SQLException e) {
				System.err.println("readBeneficiary" +e.getMessage());
				e.printStackTrace();
			}
			return list;
		}
		
		@Override
		public int updateBeneficiary(Beneficiary beneficiary) {
			try {
				pst = Db.getDbProperties().prepareStatement(Query.updateBeneficiary);
				pst.setString(1, beneficiary.getName());
				pst.setString(2, beneficiary.getAccounttype());
				pst.setString(3, beneficiary.getRid());
				result = pst.executeUpdate();
			}catch(SQLException e) {
				System.err.println("updateBeneficiary" + e.getMessage());
				e.printStackTrace();
			}
			return result;
		}
		
		@Override
		public int deleteBeneficiary(Beneficiary beneficiary) {
			try {
				pst = Db.getDbProperties().prepareStatement(Query.deleteBeneficiary);
				pst.setString(1, beneficiary.getRid());
				result=pst.executeUpdate();
			}catch (SQLException e) {
				System.err.println("deleteBeneficiary" + e.getMessage());
				e.printStackTrace();
			}
			return result;
		}
			
}	
		

