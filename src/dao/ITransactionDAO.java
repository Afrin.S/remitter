package dao;

import model.Transaction;

public interface ITransactionDAO {
	
	public int addTransaction(Transaction transaction);

}
