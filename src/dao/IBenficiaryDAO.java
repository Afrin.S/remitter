package dao;

import java.util.List;

import model.Beneficiary;

public interface IBenficiaryDAO {
	
	public int addBenficiary(Beneficiary beneficiary);

	
	public List<Beneficiary> readBeneficiary();
	
	public int updateBeneficiary(Beneficiary beneficiary);
	
	public int deleteBeneficiary(Beneficiary beneficiary);
		
	}


