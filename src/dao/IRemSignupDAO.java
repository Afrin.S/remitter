package dao;

import model.Remittersignup;

public interface IRemSignupDAO {
	
	public int addRemSignup(Remittersignup signup);
	public int updateRemSignup(Remittersignup signup);

	
}
