package dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.RemPersonal;
import util.Db;
import util.Query;

public class RemPerDAOImp implements IRemPersonalDAO {
	int result;
	PreparedStatement pst;
	@Override
	public int addRemPersonal(RemPersonal personal) {
		try {
			pst=Db.getDbProperties().prepareStatement(Query.addRemPersonal);
			pst.setString(1, personal.getRid());
			pst.setString(2, personal.getEmail());
			pst.setLong(3, personal.getMobile());
			pst.setString(4, personal.getAddress());
			result=pst.executeUpdate();
			
		}catch(SQLException e) {
			System.err.println("RemPersonal" +e.getMessage());
			e.printStackTrace();
		}
		return result;
	}
}
