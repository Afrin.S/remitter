package dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.RemLogin;
import util.Db;
import util.Query;

public class RemLDAOImp implements IRemLDAO{
	int result;
	PreparedStatement pst;
	@Override
	public int addRemLogin(RemLogin login) {
		try {
			pst=Db.getDbProperties().prepareStatement(Query.addRemLogin);
			pst.setString(1, login.getUser_name());
			pst.setString(2, login.getPassword());
			result=pst.executeUpdate();
		}catch(SQLException e) {
			System.out.println("add RemLogin" +e.getMessage());
			e.printStackTrace();
		}
		return result;
	}

}
