package dao;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.Admin;
import util.Db;
import util.Query;

public class AdminDAOImpl implements IAdminDAO{
	int result;
	PreparedStatement pst;

	@Override
	public int addAdmin(Admin admin) {
		try {
			pst=Db.getDbProperties().prepareStatement(Query.addAdmin);
			pst.setString(1, admin.getId());
			pst.setString(2, admin.getPassword());
			result=pst.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Addadmin" +e.getMessage());
			e.printStackTrace();
		}
		return result;
	}

}