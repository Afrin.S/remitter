package dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.Remittersignup;
import util.Db;
import util.Query;

public class RemSignupDAOImp implements IRemSignupDAO{
	
	int result;
	PreparedStatement pst;
	@Override
	public int addRemSignup(Remittersignup signup) {
		try {
			pst=Db.getDbProperties().prepareStatement(Query.addRemSignup);
			pst.setString(1, signup.getRid());
			pst.setString(2, signup.getName());
			pst.setLong(3, signup.getAccount_number());
			pst.setLong(4, signup.getAccount_balance());
			pst.setString(5, signup.getAccount_type());
			pst.setString(6, signup.getPassword());
			pst.setString(7, signup.getRole());
			result=pst.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Remittersignup" +e.getMessage());
			e.printStackTrace();
		}
		return result;
	}
	
	@Override
	public int updateRemSignup(Remittersignup signup) {
		try {
			pst=Db.getDbProperties().prepareStatement(Query.updateRemSignup);
			pst.setString(1, signup.getName());
			pst.setLong(2, signup.getAccount_balance());
			pst.setString(3, signup.getPassword());
			pst.setString(4, signup.getRid());
			result=pst.executeUpdate();
		} catch (SQLException e) {
			System.err.println("updateRem" +e.getMessage());
			e.printStackTrace();
		}
		return result;
	}
		
		
	}
	
	
	
	
	


