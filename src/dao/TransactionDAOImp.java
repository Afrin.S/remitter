package dao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Date;
import model.Transaction;           
import util.Db;
import util.Query;

public class TransactionDAOImp implements ITransactionDAO{
	int result;
	PreparedStatement pst;
	
	@Override
	public int addTransaction(Transaction transaction) {
		try {
			pst = Db.getDbProperties().prepareStatement(Query.addTransaction);
			pst.setLong(1, transaction.getAccount_number());
			pst.setLong(2, transaction.getTransferred_amount());
			pst.setString(3, transaction.getNarration());
			pst.setDate(4, new Date(transaction.getDate_of_transaction().getTime()));
			pst.setString(5, transaction.getStatus());
			pst.setString(6, transaction.getRid());
			result=pst.executeUpdate();
		} catch(SQLException e) {
			System.err.println("addTransaction()" +e.getMessage());
		}
		return result;
	}

}
