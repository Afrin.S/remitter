package util;

public class Query {
	//Admin
	public static String addAdmin = "insert into Admin values(?,?)";
	
	//Account
	public static String addAccount="insert into Account_Details values(?)";

	//RemitterSignup
	public static String addRemSignup="insert into Remitter_signup values(?,?,?,?,?,?,?)";
	public static String updateRemSignup="update Remitter_signup set name=?, account_balance=?,password=? where rid=?";
	
	//RemitterPersonal
	public static String addRemPersonal="insert into Remitter_Personal values(?,?,?,?)";
	public static String addRemLogin="insert into RemLogin values(?,?)";
	
	//Beneficiary
	public static String addBeneficiary="insert into Beneficiary values(?,?,?,?,?,?,?,?)";
	public static String readBeneficiary="select * from Beneficiary";
	public static String updateBeneficiary="update Beneficiary set name=?,account_type=? where rid=?";
	public static String deleteBeneficiary="delete from Beneficiary where rid=?";
	
	public static String tabular = String.format("%-10s%-5s%-5s%-10s%-10s%-10s%-10s%s\n","account_number","name","account_type","account_status","ifsc_code","email","max_limit","rid");
	
   //Transaction
	public static String addTransaction="insert into Transaction values(?,?,?,?,?,?)";
	
}
