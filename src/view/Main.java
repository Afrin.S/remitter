package view;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import controller.AccountController;
import controller.AdminController;
import controller.BeneficiaryController;
import controller.RemLcontroller;
import controller.RemPerController;
import controller.RemSignupController;
import controller.TransactionController;
import model.Beneficiary;
import util.Query;

public class Main {
	static int result;

	static String v[];

	public static void main(String[] args) throws ParseException {

			Scanner scanner = new Scanner(System.in);
			AdminController controller1 = new AdminController();
			AccountController controller2 = new AccountController();
			RemSignupController controller3 = new RemSignupController();
			RemPerController controller4 = new RemPerController();
			RemLcontroller controller5 = new RemLcontroller();
			BeneficiaryController controller6 = new BeneficiaryController();
			TransactionController controller7 = new TransactionController();
			try {
				System.out.println("    **************************************************    ");
				System.out.println("        Welcome To FUND TRANSFER BANK APPLICATION            ");
				System.out.println("    ***************************************************     ");
				System.out.println("                                                         ");
				System.out.println("  [1]Admin Login  [2]Remmitter  ");
				System.out.println("                                        ");

				Integer option = scanner.nextInt();
				scanner.nextLine();
				switch (option) {

				// Admin Login Page

				case 1:

					System.out.println("   Enter AdminId :  ");
					String user = scanner.nextLine();
					System.out.println("Enter Admin Password..");
					String pass = scanner.nextLine();
					if(user.equals("15401") && pass.equals("John@123")) {
						System.out.println(" WELCOME TO ADMIN PAGE!!!");
					System.out.println("              ");
					
						System.out.println(" Enter Your AccountNumber!!");
						v=scanner.nextLine().split(",");
						result = controller2.addAccount(Long.parseLong(v[0]));
					
					if (result > 0) {
						
						System.out.println("  Accountnumber Loggedin Successfully  ");
					} else {
						System.out.println("  Please Check AccountDetails  ");
					}
				   }else {
						System.err.println("Credentials You Entered is Incorrect Please Check Your Details!! ");
					}
					break;
					//Remitter Login Page
					
				case 2:
					System.out.println("       ***********************************      ");
					System.out.println("          Welcome To Remitter Page         ");
					System.out.println("       ************************************  ");
					System.out.println("                                         ");
					System.out.println("   Choose your option  ");
					System.out.println("                                         ");
					System.out.println("  1] Remitter Login 2]Remitter Signin  ");
					Integer option1 = scanner.nextInt();
					scanner.nextLine();
					System.out.println("   User_name, Password  ");
					v = scanner.nextLine().split(",");
					result = controller5.addRemLogin(v[0], v[1]);
					if (result > 0) {
						System.out.println("  Welcome to Remitter Login Page  ");
					} else {
						System.out.println("  Username & Password is incorrect  ");
					}
			
				switch (option1) {
				case 1:
					System.out.println("1]Beneficiary  2]Fund");
					Integer option2 = scanner.nextInt();
					scanner.nextLine();
				switch(option2) {
				case 1:
					
					//beneficiary
					System.out.println("       ********************************      ");
					System.out.println("          Welcome To Beneficiary Page         ");
					System.out.println("       *********************************  ");
					System.out.println("                                         ");
					System.out.println("   Choose your option  ");
					System.out.println("   1] Add, 2] View 3] Update 4] Delete ");
					Integer option3 = scanner.nextInt();
					scanner.nextLine();
					switch (option3) {
					case 1:
						System.out.println(
								"Account_number, Name, Account_type, Account_status, ifsc_code, email, max_limit, rid");
						v = scanner.nextLine().split(",");
						result = controller6.addBeneficiary(Long.parseLong(v[0]), v[1], v[2], v[3], v[4], v[5],
								Long.parseLong(v[6]), v[7]);
						if (result > 0) {
							System.out.println("  Beneficiary Details has Done Successfully");
						} else {
							System.out.println("  Please Check Your Details!!");
						}
						break;
					case 2:
						List<Beneficiary> list = controller6.readBeneficiary();
						if (list.size() > 0) {
							System.out.format(Query.tabular);
						for (Beneficiary beneficiary : list) {
							
						System.out.println(beneficiary);
						}
						}
						break;
					case 3:
						System.out.println("  Rid, name, email  ");
						v = scanner.nextLine().split(",");
						result = controller6.updateBeneficiary(v[0], v[1], v[2]);
						if (result > 0) {
							System.out.println("  Updated Beneficiary details successfully  ");
						} else {
							System.out.println("   Beneficiary Not Updated Sucessfully!!!  ");
						}
						break;
					case 4:
						System.out.println("   Enter Remitter id  ");
						result = controller6.deleteBeneficiary(scanner.nextLine());
						if (result > 0) {
							System.out.println("Beneficiary Details has been Deleted Successfully");

						} else {
							System.out.println("Remitter id not found");
						}
					default:
						System.err.println("Invalid Option");
					}
					break;
             //Transaction page
					
				case 2:
					
					System.out.println("       *********************************      ");
					System.out.println("          Welcome To Transaction Page         ");
					System.out.println("       **********************************  ");
					System.out.println("                                         ");
					System.out.println(
							"Transaction: account_number,transferred_amount,narration,date_of_transaction, status,rid");
					v = scanner.nextLine().split(",");
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					sdf.setLenient(false);
					Date date_of_transaction = new Date();
					date_of_transaction = sdf.parse(v[3]);
					result = controller7.addTransaction(Long.parseLong(v[0]), Long.parseLong(v[1]), v[2],
							date_of_transaction, v[4], v[5]);
					if (result > 0) {
						System.out.println("Transaction Done Successfully");
					} else {
						System.out.println("Please Check Your Details!!!");
					}
				default:
					System.out.println("Invalid option");
				}
			
				break;
				// Remitter Sign IN

				
			case 2:

					System.out.println("       ************************************      ");
					System.out.println("          Welcome To Remitter SignIN Page         ");
					System.out.println("       *************************************  ");
					System.out.println("                                         ");
					System.out.println("1]Remitter Signup  2]Remitter Registration   ");
					Integer option4 = scanner.nextInt();
					scanner.nextLine();
					
					switch (option4) {
					case 1:
						System.out.println("          Welcome To Remitter SignUp Page         ");
						System.out.println("   Choose your option!!!");
						System.out.println("                          ");
						System.out.println("   1]AddDetails 2]UpdateDetails    ");
						Integer option5 = scanner.nextInt();
						scanner.nextLine();
					switch(option5) {
					//signup here
					case 1:
						 System.out.println("  rid , name, account_number, account_balance, account_type, password,role");
						v = scanner.nextLine().split(",");
						result = controller3.addRemSignup(v[0], v[1], Long.parseLong(v[2]), Long.parseLong(v[3]), v[4],
								v[5], v[6]);
						if (result > 0) {
							System.out.println("  Remitter details has been added Successfully  ");
						} else {
							System.out.println("   Please Check Your details  ");
						}
					
						break;

					case 2:
						System.out.println("  Update Record: rid, name, accountbalance, password  ");
						v = scanner.nextLine().split(",");
						result = controller3.updateRemSignup(v[0], v[1], Long.parseLong(v[2]), v[3]);
						if (result > 0) {
							System.out.println("  Remitter Sign Up details updated successfully  ");

						} else {
							System.out.println("  Not Updated please check your Details  ");
						}
					default:
						System.err.println("   Invalid Option   ");
					}
					break;

				// Remitter personal

				case 2:

					System.out.println("    *********************************************   ");
					System.out.println("          Welcome To Remitter Registration Page  ");

					System.out.println("    *********************************************  ");
					System.out.println("                                         ");
					System.out.println("   Enter Your Details!!!  ");
					System.out.println("  rid,email,mobile,address");
						v = scanner.nextLine().split(",");
						result = controller4.addRemPersonal(v[0], v[1], Long.parseLong(v[2]), v[3]);
						if (result > 0) {
							System.out.println("  Remitter Registration  has been done Successfully");
						} else {
							System.out.println("  Check Your Details Once!!  ");
						}
					
					default:
						System.out.println("Invalid option");
					}
				}
				}
				}
	

			catch (NullPointerException | NumberFormatException | InputMismatchException ex) {
				System.err.println("Main" + ex.getMessage());
			} finally {
				scanner.close();
			}

		}
			
		}
